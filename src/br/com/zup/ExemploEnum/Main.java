package br.com.zup.ExemploEnum;

public class Main {
    public static void main(String[] args) {
        Cliente obj1 = new Cliente("André", "23485", Categoria.BASICA);
        Cliente obj2 = new Cliente("Alefe", "234524", Categoria.EXECUTIVO);
        Cliente obj3 = new Cliente("Lidiane", "345678456", Categoria.PERSONALITTE);

        Sistema.compararCategoria(obj1);
        Sistema.compararCategoria(obj2);
        Sistema.compararCategoria(obj3);


    }
}
