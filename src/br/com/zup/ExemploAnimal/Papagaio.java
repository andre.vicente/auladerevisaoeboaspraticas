package br.com.zup.ExemploAnimal;

public class Papagaio extends Animal{
    private double tamanhoDasAsas;
    private String corDasAsas;

    public Papagaio(double tamanho, double peso, double tamanhoDasAsas, String corDasAsas) {
        super(tamanho, peso);
        this.tamanhoDasAsas = tamanhoDasAsas;
        this.corDasAsas = corDasAsas;
    }

    public double getTamanhoDasAsas() {
        return tamanhoDasAsas;
    }

    public void setTamanhoDasAsas(double tamanhoDasAsas) {
        this.tamanhoDasAsas = tamanhoDasAsas;
    }

    public String getCorDasAsas() {
        return corDasAsas;
    }

    public void setCorDasAsas(String corDasAsas) {
        this.corDasAsas = corDasAsas;
    }

    @Override
    public void locomover(){
        System.out.println("Voando");
    }
}
