package br.com.zup.ExemploAnimal;

public class Cachorro extends Animal{
    private double tamanhoRabo;
    private String nome;

    public Cachorro(double tamanho, double peso, double tamanhoRabo, String nome) {
        super(tamanho, peso);
        this.tamanhoRabo = tamanhoRabo;
        this.nome = nome;
    }

    public double getTamanhoRabo() {
        return tamanhoRabo;
    }

    public void setTamanhoRabo(double tamanhoRabo) {
        this.tamanhoRabo = tamanhoRabo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void locomover(){
        System.out.println("Engatinhando");
    }
}
