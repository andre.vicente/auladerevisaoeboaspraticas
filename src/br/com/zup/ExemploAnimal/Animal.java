package br.com.zup.ExemploAnimal;

public class Animal {
    private double tamanho;
    private double peso;

    public Animal(){}
    public Animal(double tamanho, double peso) {
        this.tamanho = tamanho;
        this.peso = peso;
    }

    public double getTamanho() {
        return tamanho;
    }

    public void setTamanho(double tamanho) {
        this.tamanho = tamanho;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public void locomover(){
        System.out.println("Locomovendo");
    }
}
