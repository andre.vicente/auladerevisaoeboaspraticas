package br.com.zup.Sthefam;

import java.util.Scanner;

public class Sistema {

    public static void menu() throws Exception {

        String nome, email, telefone;

        Scanner objScanner = new Scanner(System.in);

        System.out.println("Lista de Contatos\n1 - Adicionar Contatos\n2 - Excluir Contatos\n3 - Exibir Contatos\n4 - Sair");

        int op = objScanner.nextInt();

        switch(op){
            case 1 :
                System.out.printf("Adicionar Contatos\n");
                objScanner.nextLine();
                System.out.println("Digite o nome: ");
                nome = objScanner.nextLine();
                System.out.println("Digite o e-mail: ");
                email = objScanner.next();
                System.out.println("Digite o telefone: ");
                telefone = objScanner.next();
                Agenda.adicionarContato(nome,email,telefone);
                menu();
                break;
            case 2 :
                System.out.println("Excluir Contatos");
                System.out.println("Digite o e-mail: ");
                email = objScanner.next();
                Agenda.excluirContato(email);
                menu();
                break;
            case 3 :
                Agenda.mostrarLista();
                menu();
                break;
            case 4 :
                System.exit(0);
                break;
            default:
                System.out.println("Opção Inválida!");
        }

    }

}
