package br.com.zup.Sthefam;

import javax.lang.model.util.ElementScanner6;
import java.util.ArrayList;
import java.util.List;

public class Agenda {

    private static List<Contato> contatos = new ArrayList<Contato>();

    public static void adicionarContato(String nome, String email, String telefone) throws Exception {
        for(Contato contato : contatos){
            if(contato.getEmail().equals(email)){
                throw new Exception("\nEsse e-mail já existe!\n");
            }
        }
        contatos.add(new Contato(nome,email,telefone));
        System.out.println("\nContato cadastrado com sucesso!\n");
    }

    public static void mostrarLista() throws Exception {
        if(contatos.size() == 0){
            throw new Exception("\nNenhum contato encontrado!\n");
        } else {
            System.out.println("\nExibindo "+contatos.size()+" contato(s)\n");
            for(Contato c : contatos){
                System.out.println(c);
            }
        }
    }

    public static void excluirContato(String email) throws Exception {
        if(contatos.removeIf(contato -> contato.getEmail().equals(email))){
            System.out.println("\nContato excluido com sucesso!\n");
        } else {
            throw new Exception("\nContato não encontrado!\n");
        }
    }

}
