package br.com.zup.ExemploSistemaRestaurante;

import java.util.ArrayList;
import java.util.List;

public class Cardapio {
    private static List <Prato> pratos = new ArrayList<>();

    public static void adicionarPratosNoCardapio(Prato prato){
        pratos.add(prato);
    }

    public static void mostrarCardapio(){
        System.out.println("Pratos disponíveis");
        for (Prato percorredor: pratos ) {
            System.out.println(percorredor);
        }
    }
    public static void excluirIngredienteDeUmPrato(String nomeDoPrato, String nomeDoIngrediente){
        for (Prato percorredor : pratos){
            if (percorredor.getNome().equals(nomeDoPrato)){
                percorredor.removeIngredienteDaLista(nomeDoIngrediente);
            }
        }
    }

}
