package br.com.zup.ExemploSistemaRestaurante;

import java.util.ArrayList;
import java.util.List;

public class Prato {
    private String nome;
    private double valor;
    private List <Ingrediente> ingredientes = new ArrayList<>();

    public Prato(){}

    public Prato(String nome, double valor, List<Ingrediente> ingredientes) {
        this.nome = nome;
        this.valor = valor;
        this.ingredientes = ingredientes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public void adicionarIngrediente(Ingrediente ingrediente){
        ingredientes.add(ingrediente);
    }
    public void removeIngredienteDaLista(String nome){
        Ingrediente ingredienteDeRetorno = null;
        for (Ingrediente percorredor : ingredientes){
            if (percorredor.getNome().equals(nome)){
                ingredienteDeRetorno = percorredor;
            }
        }
        ingredientes.remove(ingredienteDeRetorno);
    }

    public Ingrediente pegarIngredienteDoPrato(String nomeDoIngrediente){
        Ingrediente ingredienteDeRetorno = null;
        for (Ingrediente percorrer : ingredientes){
            if (percorrer.getNome().equals(nomeDoIngrediente)){
                ingredienteDeRetorno = percorrer;
            }
        }
        return ingredienteDeRetorno;
    }

    @Override
    public String toString() {
        return "Pratos{" +
                "nome='" + nome + '\'' +
                ", valor=" + valor +
                ", ingredientes=" + ingredientes +
                '}';
    }
}
