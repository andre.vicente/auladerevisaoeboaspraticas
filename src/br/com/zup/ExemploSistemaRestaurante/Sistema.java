package br.com.zup.ExemploSistemaRestaurante;

import br.com.zup.ListaTelefonica.ListaDeContatos;

import java.util.ArrayList;
import java.util.List;
public class Sistema {
    // Criamos um método para adicionar um ingrediente
    public static Ingrediente cadastrarIngrediente(){
        System.out.println("Digite o nome do ingrediente: ");
        String nome = IO.criaScanner().nextLine();
        System.out.println("Ditite o valor do ingrediente: ");
        double valor = IO.criaScanner().nextDouble();
        Ingrediente ingrediente = new Ingrediente(nome, valor);
        return ingrediente;

    }   // Criamos um método pra cadastrar um prato
    public static void cadastrarPrato(){
        List <Ingrediente> ingredientes = new ArrayList<>();

        System.out.println("Digite o nome do prato: ");
        String nome = IO.criaScanner().nextLine();

        System.out.println("Digite o valor do prato: ");
        double valor = IO.criaScanner().nextDouble();

        System.out.println("Digite a quantidade de ingredientes");
        double qtdIngrediente = IO.criaScanner().nextDouble();

        for (int quantidade = 0; quantidade < qtdIngrediente; quantidade++) {
            ingredientes.add(cadastrarIngrediente());
        }
        // Instanciamos um prato com os valores recebidos acima
        Prato prato = new Prato(nome, valor, ingredientes);
        Cardapio.adicionarPratosNoCardapio(prato);
    }

    public static void exluirIngrediente(){
        System.out.println("Vamos excluir o ingrediente da lista");
        System.out.println("Digite o nome do prato: ");
        String nomeDoPrato = IO.criaScanner().nextLine();
        System.out.println("Digite o nome do ingrediente: ");
        String nomeDoIngrediente = IO.criaScanner().nextLine();
        Cardapio.excluirIngredienteDeUmPrato(nomeDoPrato, nomeDoIngrediente);
    }
    public static void menu(){
        System.out.println("SEJA BEM VINDO AO ANDRÉ RESTAURANTE");
        System.out.println("Digite 1 para cadastrar um prato: ");
        System.out.println("Digite 2 para excluir um prato: ");
        System.out.println("Digite 3 para mostrar o cardápio: ");
        System.out.println("Digite 4 para sair do programa: ");
    }
    // Metodo responsável por executar os métodos do sistema
    public static void executarSistema(){
        boolean executar = true;

        while (executar == true) {
            menu();
            String opcao = IO.criaScanner().nextLine();

            switch (opcao) {
                case "1":
                    cadastrarPrato();
                    break;
                case "2":
                    exluirIngrediente();
                    break;
                case "3":
                    Cardapio.mostrarCardapio();
                    break;
                case "4":
                    executar = false;
                    break;
            }
        }
    }
}
