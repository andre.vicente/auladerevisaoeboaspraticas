package br.com.zup.ListaTelefonica;

public interface InterfaceContato {
    public String getNome();
    public void setNome(String nome);
    public String getTelefone();
    public void setTelefone(String telefone);
    public String getEmail();
    public void setEmail(String email);

}
