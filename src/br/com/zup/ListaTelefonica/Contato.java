package br.com.zup.ListaTelefonica;

public class Contato implements InterfaceContato{
    private String nome;
    private String telefone;
    private String email;

    public Contato(){}

    public Contato(String nome, String telefone, String email) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
    }
@Override
    public String getNome() {
        return nome;
    }
    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }
    @Override
    public String getTelefone() {
        return telefone;
    }
    @Override
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    @Override
    public String getEmail() {
        return email;
    }
    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("O nome do contato é "+nome);
        string.append("\nO telefone do contato é "+telefone);
        string.append("\nO email do contato é "+email);
        return string.toString();
    }
}
