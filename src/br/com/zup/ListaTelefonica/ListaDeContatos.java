package br.com.zup.ListaTelefonica;

import java.util.ArrayList;
import java.util.List;

public class ListaDeContatos{
    private static List <Contato> contatos= new ArrayList<>();

    public static void mostrarListaDeContatos(){
        System.out.println("Mostrar lista de contatos: ");
        for (Contato contato : contatos){
            System.out.println(contato);
        }
    }

    public static void validarContatoPorEmail(String email) throws Exception{
        for (Contato contato:contatos) {
            if (contato.getEmail().equals(email)) {
                throw new Exception("Não é possível cadastrar o contato, pois já existe um contato com o mesmo e-mail");
            }
        }

    }

    public static void adicionarContato(Contato contato) throws Exception{
        validarContatoPorEmail(contato.getEmail());
        contatos.add(contato);
    }

    public static void excluirContato(String email) throws Exception{
        Contato contatoASerDeletado = null;
        for (Contato contato : contatos){
            if (contato.getEmail().equals(email)){
                System.out.println("Contato excluido com sucesso da lista de contatos!");
                contatoASerDeletado = contato;
            }
        }
        contatos.remove(contatoASerDeletado);
        throw new Exception("Não é possível excluir mail não cadastrado");
    }

}
