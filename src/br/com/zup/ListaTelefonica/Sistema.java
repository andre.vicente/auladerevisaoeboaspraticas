package br.com.zup.ListaTelefonica;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Sistema {
    public static void cadastrarContato() throws Exception{
        IO.mostrarTexto("Por favor, digite o nome do contato: \n");
        String nome = IO.criaScanner().next();
        IO.mostrarTexto("Por favor, digite o telefone do contato: \n");
        String telefone = IO.criaScanner().next();
        IO.mostrarTexto("Por favor, digite o email do contato: ");
        String email = IO.criaScanner().next();

        Contato contato = new Contato(nome, telefone, email);
        ListaDeContatos.adicionarContato(contato);
    }

    public static void excluirContatoPorEmail() throws Exception{
        System.out.println("Digite o email do contato que você quer exlcuir da lista");
        String email = IO.criaScanner().nextLine();
        ListaDeContatos.excluirContato(email);
    }

    public static void menu(){
        System.out.println("-------------");
        System.out.println("Digite 1 para cadastrar um contato");
        System.out.println("Digite 2 para exibir os contatos cadastrados");
        System.out.println("Digite 3 para excluir um contato da lista");
        System.out.println("Digite 4 para sair do programa");
        System.out.println("-------------");
    }

    public static void executarSistema() throws Exception{
        boolean executar = true;

        while (executar == true){
            menu();
            String opcaoEscolhida = IO.criaScanner().next();

            switch (opcaoEscolhida){
                case "1":
                    cadastrarContato();
                    break;

                case "2":
                    ListaDeContatos.mostrarListaDeContatos();
                    break;

                case "3":
                    excluirContatoPorEmail();
                    break;

                case "4":
                    executar = false;
                    break;
            }

        }
    }
}
