package br.com.zup.ExemploBanco;

public class ContaPJ extends ContaBancaria{
    private double emprestimo;

    public ContaPJ(double agencia, double numeroDaConta, double saldo, double emprestimo) {
        super(agencia, numeroDaConta, saldo);
        this.emprestimo = emprestimo;
    }

    public double getEmprestimo() {
        return emprestimo;
    }

    public void setEmprestimo(double emprestimo) {
        this.emprestimo = emprestimo;
    }

    public void utilizarEmprestimo(double valorEmprestimo){
        this.emprestimo -= valorEmprestimo;
        this.setSaldo(this.getSaldo() + valorEmprestimo);
    }

    @Override
    public String toString() {
        StringBuilder retorno = new StringBuilder();
        retorno.append("\n A agencia da conta é: " +getAgencia());
        retorno.append("\n O numero da conta é: "+getNumeroDaConta());
        retorno.append("\n O saldo da conta é: "+getSaldo());
        retorno.append("\n O emprestimo disponível é de: "+emprestimo);
        return retorno.toString();
    }
}
