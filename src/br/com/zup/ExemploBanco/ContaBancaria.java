package br.com.zup.ExemploBanco;

public class ContaBancaria {
    private double agencia;
    private double numeroDaConta;
    private double saldo;

    public ContaBancaria(){}

    public ContaBancaria(double agencia) {
        this.agencia = agencia;
    }

    public ContaBancaria(double agencia, double numeroDaConta, double saldo) {
        this.agencia = agencia;
        this.numeroDaConta = numeroDaConta;
        this.saldo = saldo;
    }

    public double getAgencia() {
        return agencia;
    }

    public void setAgencia(double agencia) {
        this.agencia = agencia;
    }

    public double getNumeroDaConta() {
        return numeroDaConta;
    }

    public void setNumeroDaConta(double numeroDaConta) {
        this.numeroDaConta = numeroDaConta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    // De forma explicita
    public void sacar(double valorSaque){
        this.saldo = this.saldo - valorSaque;
    }

   // De forma implicita
    public void depositar(double valorDeposito){
        this.saldo += valorDeposito;
    }
}
