package br.com.zup.ExemploBanco;

public class ContaCorrente extends ContaBancaria{
    private double creditoPessoal;

    public ContaCorrente(double agencia, double numeroDaConta, double saldo, double creditoPessoal) {
        super(agencia, numeroDaConta, saldo);
        this.creditoPessoal = creditoPessoal;
    }

    public void utilizarCreditoPessoal(double valorCredito){
        this.creditoPessoal -= valorCredito;
        this.setSaldo( this.getSaldo() + valorCredito);
    }



    @Override
    public String toString() {
      StringBuilder retorno = new StringBuilder();
      retorno.append("\n A agencia da conta é: " +getAgencia());
      retorno.append("\n O numero da conta é: "+getNumeroDaConta());
      retorno.append("\n O saldo da conta é: "+getSaldo());
      retorno.append("\n O Crédito disponível é de: "+creditoPessoal);
      return retorno.toString();
    }
}
